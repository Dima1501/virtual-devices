"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(document).ready(function () {

    if ($('#map').length > 0) {

        var myMapPlace = document.getElementById("map"),
            myLatlng = new google.maps.LatLng(59.93074319999999, 30.36183200000005),
            myOptions = _defineProperty({
            scrollwheel: false,
            disableDefaultUI: true,
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true
        }, "mapTypeControl", false),
            map = new google.maps.Map(myMapPlace, myOptions);

        var styles = [{
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#444444"
            }]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "color": "#f2f2f2"
            }]
        }, {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "poi.park",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#c1e9ae"
            }, {
                "visibility": "on"
            }]
        }, {
            "featureType": "poi.park",
            "elementType": "labels",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
            }, {
                "lightness": 45
            }]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{
                "visibility": "simplified"
            }]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [{
                "color": "#ffffff"
            }]
        }, {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{
                "color": "#aadaff"
            }, {
                "visibility": "on"
            }, {
                "lightness": "10"
            }]
        }];

        map.setOptions({ styles: styles });

        var companyLogo = new google.maps.MarkerImage('assets/images/static/map-marker.svg', new google.maps.Size(70, 80), new google.maps.Point(0, 0), new google.maps.Point(35, 35));

        var offices = [{
            ID: 1,
            LAT: 59.93074319999999,
            LNG: 30.36183200000005,
            LOGO: companyLogo,
            ADDR: 'ул. Восстания 3б',
            PHONE: '+7 (812) 389-21-60'
        }],
            coords = [],
            markers = [],
            infoboxes = [];

        for (var i = 0; i < offices.length; i++) {
            coords[i] = new google.maps.LatLng(eval(offices[i].LAT), eval(offices[i].LNG));
            markers[i] = new google.maps.Marker({

                position: coords[i],
                map: map,
                icon: offices[i].LOGO,
                addr: offices[i].ADDR,
                phone: offices[i].PHONE,
                id: offices[i].ID,
                i: i,
                optimized: false,
                zIndex: 10

            });
        };
    };
});