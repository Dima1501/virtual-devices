$(document).ready(function () {

	if ($('#p-map').length > 0) {

        var myMapPlace = document.getElementById("p-map"),

        myLatlng = new google.maps.LatLng(59.930743, 30.361832),

        myOptions = {
            scrollwheel: false,
            disableDefaultUI: true,
            zoom: 3,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true,
            mapTypeControl: false
        },

        map = new google.maps.Map(myMapPlace, myOptions);

        var styles = [
    {
        "featureType": "all",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#effafd"
            },
            {
                "gamma": "1.00"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.attraction",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }
]

        map.setOptions({styles: styles});

        var companyLogo = new google.maps.MarkerImage('assets/images/static/p-map-marker.svg',
                new google.maps.Size(70, 70),
                new google.maps.Point(0, 0),
                new google.maps.Point(35, 35));

        var offices = [
	        {
	            ID: 1,
	            LAT: 59.930743,
	            LNG: 30.361832,
	            LOGO: companyLogo,
	            ADDR: 'ул. Восстания 3б',
	            PHONE: '+7 (812) 389-21-60'
	        }
        ],
        coords =  [],
        markers = [],
        infoboxes = [];

        for (var i = 0; i < offices.length; i++){
            coords[i] =  new google.maps.LatLng(eval(offices[i].LAT), eval(offices[i].LNG));
            markers[i] = new google.maps.Marker({

                position: coords[i],
                map: map,
                icon: offices[i].LOGO,
                addr: offices[i].ADDR,
                phone: offices[i].PHONE,
                id: offices[i].ID,
                i: i,
                optimized: false,
                zIndex: 10

            });
        };
    };
});