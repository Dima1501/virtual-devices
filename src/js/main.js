$(document).ready(function() {

	wSlider();

	iSlider();

	callbackFormValidate();

	phoneMask();

	headerScroll();

	svgAdvantages();

	filtr();

	popupFormValidate();

	popups();

	headerSearch();

	pSlider();

	rSlider();

	sections();

	pLinks();

	hSlider();

	servSvg();

	videos();

	flySvg();

	mapParallax();

	svgParallax();
});

let wSlider = () => {

	if ($('.w-slider').length > 0) {

		let timer, timer1, timer2, timer3;

		let Slider = function Slider() {

			var wrapper = $('.w-slider');

			var currentSlide = $('.w-slider__main--active');

			TweenMax.from(currentSlide, 0.8, { x: -500 });

			this.next = function () {

				var currentSlide = $('.w-slider__main--active');
				var nextSlide = $('.w-slider__main--next');
				var slidesLength = $('.w-slider__main').length;
				var activeIndex = $('.w-slider__main--active').index() + 1;

				TweenMax.to(currentSlide, 0.5, { opacity: 0 });

				svg.change();

				$('.w-slider__announce-text').fadeOut();

				setTimeout(function () {

					var currentSlide = $('.w-slider__main--active');

					currentSlide.removeClass('w-slider__main--active');

					if (activeIndex + 1 == slidesLength) {

						var nextText = $('.w-slider__main').eq(0).find('.w-slider__title').text();

					} else {

						var nextText = $('.w-slider__main').eq(activeIndex + 1).find('.w-slider__title').text();
					}

					if (activeIndex == slidesLength) {

						var nextText = $('.w-slider__main').eq(1).find('.w-slider__title').text();

						$('.w-slider__main').eq(0).addClass('w-slider__main--active');

					} else {

						currentSlide.next('.w-slider__main').addClass('w-slider__main--active');
					}

					$('.w-slider__announce-text').text(nextText);

					var currentSlide = $('.w-slider__main--active');

					setTimeout(function () {

						$('.w-slider__announce-text').fadeIn();

					}, 200);

					currentSlide.fadeIn();

					TweenMax.fromTo(currentSlide, 0.8, { x: 500, opacity: 0 }, { x: 0, opacity: 1 });
					
				}, 600);
			};

			this.restart = () => {

				clearInterval(timer);

				timer = setInterval(function () {

					slider.next();

				}, 5000);
			}
		};

		function svgControl() {

			var svg2 = $('.svg__item--2');

			var svg3 = $('.svg__item--3');

			this.start = function () {

				new Vivus('svg_1', {duration: 150, start: 'autostart'});

				setTimeout(function () {

					TweenMax.fromTo(svg2, 0.3, { opacity: 0 }, {opacity: 1});

					new Vivus('svg_2', {duration: 150, start: 'autostart'});

				}, 1000);
			}

			this.change = function () {

				if ($('.svg__item--2').hasClass('drawn')) {
					
					TweenMax.fromTo(svg2, 0.3, { opacity: 1 }, {opacity: 0});

					timer1 = setTimeout(function () {

						TweenMax.fromTo(svg3, 0.3, { opacity: 0 }, {opacity: 1});

						new Vivus('svg_3', {duration: 150, start: 'autostart'});

						$('.svg__item--2').removeClass('drawn');

						$('.svg__item--3').addClass('drawn');

					}, 350);

				} else {

					TweenMax.fromTo(svg3, 0.3, { opacity: 1 }, {opacity: 0});

					timer2 = setTimeout(function () {

						TweenMax.fromTo(svg2, 0.4, { opacity: 0 }, {opacity: 1});
						
						new Vivus('svg_2', {duration: 150, start: 'autostart'});

						$('.svg__item--3').removeClass('drawn');

						$('.svg__item--2').addClass('drawn');

					}, 350);
				}
			}
		}

		var svg = new svgControl();

		var slider = new Slider();

		svg.start();

		slider.restart();

		$('body').on('click', '.w-slider__announce', function () {

			clearInterval(timer);

			clearInterval(timer1);

			clearInterval(timer2);

			slider.next();

			slider.restart();
		});
	}
}

let iSlider = () => {

	if ($('.i-slider').length > 0) {

		let carousel = $('.i-slider__scene');

		carousel.owlCarousel({
			items: 1,
			onTranslate: changeSlider
		});

		function changeSlider(event) {

			let curItem = event.item.index;

			$('.i-slider__dot').removeClass('is-active');

			$('.i-slider__dot').eq(curItem).addClass('is-active');
		}

		$('body').on('click', '.i-slider__dot-circle', function () {

			let needItem = $(this).parent().index();

			$('.i-slider__dot').removeClass('is-active');
			$(this).parent('.i-slider__dot').addClass('is-active');

			carousel.trigger('to.owl.carousel', needItem);
		});
	}
};

let callbackFormValidate = () => {

    if ($('#callbackForm').length > 0) {

        var form = $('#callbackForm');

        $('body').on('keypress', '.c-form__input', function () {

        	checkValidated()
        });

        $('body').on('click', '.c-form__button', function () {

        	checkValidated()
        });

        function checkValidated() {

        	if ($('.c-form__input.input-error').length > 0 || $('.c-form__input.valid').length < 1 ) {

        		$('.c-form__error').addClass('is-visible');

        	} else {
        		
				$('.c-form__error').removeClass('is-visible');
        	}
        }

        form.validate({
            errorElement: 'span',
            errorClass: 'input-error',

            submitHandler: function () {

            	$('.popup').removeClass('popup--visible');

                $('#successPopup').addClass('popup--visible');

                var counter = 5;

                $('.popup__timer').text(counter);

                var interval = setInterval(function () {

                	counter -= 1;

                	$('.popup__timer').text(counter);

                	switch(true) {

                		case (counter < 0):
						$('.popup__seconds').text('секунд');
						break;

						case (counter < 2):
						$('.popup__seconds').text('секунду');
						break;

						case (counter < 5): 
						$('.popup__seconds').text('секунды');
						break;

						default:
						$('.popup__seconds').text('секунд');
						break;
					}

                	if (counter == 0) {

                		$('.popup').removeClass('popup--visible');

                		clearTimeout(interval);
                	}
                }, 1000);
            }
        });

        $(form).find('.validateCompanyName').rules('add', {

            required: true,

            messages: {
                required: 'Пожалуйста, заполните все необходимые поля.',
            }
        });

        $(form).find('.validateEmail').rules('add', {

            email: true,
            required: true,

            messages: {
                required: 'Пожалуйста, заполните все необходимые поля.'
            }
        });

        $(form).find('.validateName').rules('add', {

            required: true,

            messages: {
                required: 'Пожалуйста, заполните все необходимые поля.',
            }
        });

        $(form).find('.validatePhone').rules('add', {

            required: true,

            messages: {
                required: 'Пожалуйста, заполните все необходимые поля.',
            }
        });
    }
}

let popupFormValidate = () => {

    if ($('#popupForm').length > 0) {

        var form = $('#popupForm');

        $('body').on('keypress', '.c-form__input', function () {

        	checkValidated()
        });

        $('body').on('click', '.c-form__button', function () {

        	checkValidated()
        });

        function checkValidated() {

        	if ($('.c-form__input.input-error').length > 0 || $('.c-form__input.valid').length < 1 ) {

        		$('.c-form__error').addClass('is-visible');

        	} else {
        		
				$('.c-form__error').removeClass('is-visible');
        	}
        }

        form.validate({
            errorElement: 'span',
            errorClass: 'input-error',

            submitHandler: function () {

            	$('.popup').removeClass('popup--visible');

                $('#successPopup').addClass('popup--visible');

                var counter = 5;

                $('.popup__timer').text(counter);

                var interval = setInterval(function () {

                	counter -= 1;

                	$('.popup__timer').text(counter);

                	switch(true) {

                		case (counter < 0):
						$('.popup__seconds').text('секунд');
						break;

						case (counter < 2):
						$('.popup__seconds').text('секунду');
						break;

						case (counter < 5): 
						$('.popup__seconds').text('секунды');
						break;

						default:
						$('.popup__seconds').text('секунд');
						break;
					}

                	if (counter == 0) {

                		$('.popup').removeClass('popup--visible');

                		clearTimeout(interval);
                	}

                }, 1000);
            }
        });

        $(form).find('.validateCompanyName').rules('add', {

            required: true,

            messages: {
                required: 'Пожалуйста, заполните все необходимые поля.',
            }
        });

        $(form).find('.validateEmail').rules('add', {

            email: true,
            required: true,

            messages: {
                required: 'Пожалуйста, заполните все необходимые поля.'
            }
        });

        $(form).find('.validateName').rules('add', {

            required: true,

            messages: {
                required: 'Пожалуйста, заполните все необходимые поля.',
            }
        });

        $(form).find('.validatePhone').rules('add', {

            required: true,

            messages: {
                required: 'Пожалуйста, заполните все необходимые поля.',
            }
        });
    }
}

let phoneMask = () => {

	if ($('.phone').length > 0) {

        $(".phone").mask('+7(999)-999-99-99');

    }
}

let headerScroll = () => {

	var position;

	$(window).on('scroll', function () {

        let scrolled = $(window).scrollTop();

        if (scrolled > 50) {

            $('.header').addClass('header--scrolled');

        } else {

            $('.header').removeClass('header--scrolled');
        }

	    if (scrolled > position && scrolled > 50) {

	    	$('.header').addClass('header--hidden');

	    } else {
	    	
	    	$('.header').removeClass('header--hidden');
	    }

	    position = scrolled;
	});
}


let svgAdvantages = () => {

	if ($('.advantages').length > 0) {

		function svg() {

			var myVivus;

			this.start = function (svgId) {

				$('#' + svgId).fadeIn();

				myVivus = new Vivus(svgId, {duration: 150, start: 'autostart'});
			}

			this.stop = function () {

				$('.advantages__svg .svg__item svg').fadeOut();

				myVivus.stop();

				myVivus.reset();
			}
		}

		let svgControl = new svg();

		$('.advantage').on('mouseenter', function () {

			let index = $(this).attr('data-type');

			let svgId = 'svg_advantage_' + index;

			svgControl.start(svgId);
		});

		$('.advantage').on('mouseleave', function () {

			svgControl.stop();
		});
	}
}


let filtr = () => {

	if ($('.filtr').length > 0) {

		$('body').on('click', '.filtr__reset', function () {

			$('.filtr__item-input').prop('checked',false);
		});

		$('body').on('click', '.filtr__top', function () {

			$('.filtr__inner').slideToggle();

			$('.filtr').toggleClass('is-opened');
		});
	}
}

let popups = () => {

	if ($('.popup').length > 0) {

		$('body').on('click', '[data-popup]', function () {

            var needPopup = $(this).attr('data-popup');

            $('#' + needPopup).addClass('popup--visible');

            return false;
        });

        $('body').on('click', '.popup__closer', function () {

            $('.popup').removeClass('popup--visible');

        });

        $(document).keyup(function(e) {

            if (e.keyCode === 27) {

                $('.popup').removeClass('popup--visible');
            }
        });

        $(document).on('click', function (e) {

            var block = $('.popup__inner');

            if (!block.is(e.target) &&
                block.has(e.target).length === 0) {

                $('.popup').removeClass('popup--visible');
            }
        });
	}
}

let headerSearch = () => {

	$('body').on('click', '.js-search', function () {

		$('.h-search').toggleClass('h-search--opened');

		$('body').toggleClass('is-unscrolled');

		return false;
	});

	$('body').on('click', '.h-search__closer', function() {

		$('.h-search').removeClass('h-search--opened');

		$('body').removeClass('is-unscrolled');
	});

	$(document).keyup(function(e) {

        if (e.keyCode === 27) {

            $('.h-search').removeClass('h-search--opened');

			$('body').removeClass('is-unscrolled');
        }
    });
}

let pSlider = () => {

	if ($('.p-slider').length > 0) {

		$('.p-slider__scene').each(function () {

			let that = $(this);

			let navContainer = that.parents('.p-slider').find('.p-slider__controls');

			let dotsContainer = that.parents('.p-slider').find('.p-slider__dots');

			let currentSlide = that.parents('.p-slider').find('.p-slider__counter-current');

			let slidesLength = that.parents('.p-slider').find('.p-slider__counter-value');

			that.owlCarousel({
				items: 1,
				nav: true,
				dots: true,
				dotsContainer: dotsContainer,
				navText: '',
				navElement: 'a',
				navContainer: navContainer,
				navClass: ['p-slider__arrow p-slider__arrow--prev', 'p-slider__arrow p-slider__arrow--next'],
				onTranslate: owl,
				onInitialized: owl
			});

			function owl(event) {

				currentSlide.text(event.item.index + 1);

				slidesLength.text(event.item.count);

				let nextImage = $('.p-slider__image').eq(event.item.index);

				that.parents('.p-slider').find('.p-slider__image').removeClass('is-active');

				that.parents('.p-slider').find('.p-slider__image').eq(event.item.index).addClass('is-active');
			}
		});
	}
}

let rSlider = () => {

	if ($('.r-slider').length > 0) {

		$('.r-slider__scene').each(function () {

			let that = $(this);

			let dotsContainer = that.parents('.r-slider').find('.r-slider__dots');

			let currentSlide = that.parents('.r-slider').find('.r-slider__counter-current');

			let slidesLength = that.parents('.r-slider').find('.r-slider__counter-value');

			that.owlCarousel({
				items: 1,
				nav: true,
				navText: '',
				dots: true,
				dotsContainer: dotsContainer,
				navClass: ['r-slider__arrow r-slider__arrow--prev', 'r-slider__arrow r-slider__arrow--next'],
				onTranslate: owl,
				onInitialized: owl
			});

			function owl(event) {

				currentSlide.text(event.item.index + 1);

				slidesLength.text(event.item.count);

				let nextImage = $('.r-slider__image').eq(event.item.index);

				that.parents('.r-slider').find('.r-slider__image').removeClass('is-active');

				that.parents('.r-slider').find('.r-slider__image').eq(event.item.index).addClass('is-active');
			}
		});
	}
}

let sections = () => {

	if ($('.section-title').length > 0) {

		let flyTitle = $('.section-title .s-name');

		$(window).on('scroll', function (){

			let scrolled = $(window).scrollTop();

			$('.js-section').each(function () {

				if (scrolled > $(this).offset().top - 300 && scrolled < $(this).offset().top + ($(this).height() - 300) && $(this).find('.s-name').text() !== $('.section-title .s-name').text()) {

					let title = $(this).find('.s-name').text();

					let classArray = $(this).find('.s-name').attr("class").split(' ');

					let needClass = classArray[classArray.length - 1];

					let string = '--mobile';

					flyTitle.fadeOut(100, function () {

						flyTitle.text(title);

						flyTitle.fadeIn();

						if (!needClass.includes(string)) {

							flyTitle.removeClass('s-name--grey');
							flyTitle.removeClass('s-name--white');
							
							flyTitle.addClass(needClass);
						}

					});
				}
			});
		});
	}
}

let pLinks = () => {

	if ($('.p-links').length > 0) {

		var timer;

		$('.p-link').mouseenter(function () {

			let needItem = this.className.split(' ')[1];

			let replacedClass = needItem.replace('p-link', '');

			clearTimeout(timer);

			$('.p-block' + replacedClass).addClass('is-opened');

			$(this).addClass('p-link--opened');
		});

		$('.p-block').mouseenter(function () {

			clearTimeout(timer);
		});

		$('.p-block').mouseleave(function () {

			timer = setTimeout(function () {

				$('.p-block').removeClass('is-opened');

				$('.p-link').removeClass('p-link--opened');

			}, 200);
		});

		$(window).on('scroll', function () {

			let scrolled = $(window).scrollTop();

			if (scrolled > $('.start').height() / 2) {

				$('.p-link').addClass('p-link--colored');

			} else {

				$('.p-link').removeClass('p-link--colored');
			}
		});
	}
}

let servSvg = () => {

	if ($('.services__svg').length > 0) {

		function svg() {

			var myVivus;

			this.start = function (svgId) {

				$('#' + svgId).fadeIn();

				myVivus = new Vivus(svgId, {duration: 150, start: 'autostart'});
			}

			this.stop = function () {

				$('.services__svg .svg__item svg').fadeOut();

				myVivus.stop();

				myVivus.reset();
			}
		}

		let svgControl = new svg();

		$('.services__item').on('mouseenter', function () {

			let index = $(this).attr('data-svg');

			let svgId = 'serv_' + index;

			svgControl.start(svgId);
		});

		$('.services__item').on('mouseleave', function () {

			svgControl.stop();
		});
	}
}

let hSlider = () => {

	if ($('.h-slider').length > 0) {

		$('.h-slider__scene').each(function () {

			let navContainer = $(this).parents('.h-slider').find('.h-slider__nav');

			$(this).owlCarousel({
				items: 5,
				dots: false,
				slideBy: 2,
				nav: true,
				navText: '',
				navElement: 'a',
				navContainer: navContainer,
				navClass: ['h-slider__arr h-slider__arr--prev','h-slider__arr h-slider__arr--next']
			});
		});
	}
}

let videos = () => {

	if ($('.js-play').length > 0) {

		$('.js-play').YouTubePopUp();
	}
}

let flySvg = () => {

	if ($('.advantages').length > 0) {

		let offset = $('.advantages').offset().top - 150;

		$(window).on('scroll', function () {

			let scrolled = $(window).scrollTop();

			if (scrolled > $('.advantages').offset().top - 150 
				&& scrolled < $('.advantages').offset().top + $('.advantages').height() - 300) {
				
				let svg = $('.advantages__svg');

				TweenMax.to(svg, 0, {y: scrolled - offset, force3D: true});
			}
		});
	}
}

let mapParallax = () => {

	if ($('#p-map').length > 0) {

		let map = $('.p-options__map');

		let offset = $('.p-options__map').offset().top;

		$(window).on('scroll', function () {

			let scrolled = $(window).scrollTop();

			if (scrolled > $('.p-options').offset().top) {

				TweenMax.to(map, 0, {y: scrolled * 0.7 - 560 , force3D: true});
			}
		});
	}
}

let svgParallax = () => {

	if ($('.s-card__svg').length > 0) {

		let block = $('.s-card__svg svg');

		$(window).on('scroll', function () {

			let scrolled = $(window).scrollTop();

			if (scrolled + $(window).height() > $('.s-card--svg').offset().top) {
				
				TweenMax.to(block, 0.3, {y: -1 * (scrolled * 0.1 + 140) , force3D: true});
			}
		});
	}

	if ($('.activity__svg').length > 0) {

		let block = $('.activity__svg svg');

		$(window).on('scroll', function () {

			let scrolled = $(window).scrollTop();

			if (scrolled + $(window).height() > $('.activity__svg svg').offset().top) {
				
				TweenMax.to(block, 0.3, {y: -1 * (scrolled * 0.1 - 200) , force3D: true});
			}
		});
	}
}
























